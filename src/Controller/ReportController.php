<?php

namespace Drupal\msacr\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for the msacr module.
 */
final class ReportController extends ControllerBase {

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The Guzzle http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(ModuleExtensionList $module_extension_list, Client $client, MessengerInterface $messenger) {
    $this->moduleExtensionList = $module_extension_list;
    $this->client = $client;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('extension.list.module'),
      $container->get('http_client'),
      $container->get('messenger')
    );
  }

  /**
   * The main report page.
   *
   * @return array
   *   Renderable array.
   */
  public function reportPage() {
    $modules = $this->moduleExtensionList->getList();
    $not_covered = [];
    $covered = [];
    $unknown = [];
    $checked_projects = [];
    foreach ($modules as $project_key => $module) {
      if ($module->status == 1 && $module->origin !== 'core' && $module->info['package'] !== 'Testing') {
        if (!empty($module->info['project'])) {
          if (in_array($module->info['project'], $checked_projects)) {
            continue;
          }
          $checked_projects[] = $module->info['project'];
        }
        if (!empty($module->info['version'])) {
          $version = $module->info['version'];
          $response = $this->client->get('https://updates.drupal.org/release-history/' . $project_key . '/all');
          $body = $response->getBody();
          $y = $body->read(9999999999);
          $xml = simplexml_load_string($y);
          if (!$xml) {
            $this->messenger->addWarning('unable to load release xml for ' . $project_key);
            continue;
          }
          // Try to find the release for the used version.
          $found = FALSE;
          if (!empty($xml->releases->release)) {
            foreach ($xml->releases->release as $release) {
              if ($release->version == $version) {
                $found = TRUE;
                if ($release->security == 'Covered by Drupal\'s security advisory policy') {
                  $covered[] = [
                    $project_key,
                    $version,
                    $this->t('Opted-in to security advisory coverage'),
                  ];
                }
                else {
                  $not_covered[] = [
                    $project_key,
                    $version,
                    $release->security,
                  ];
                }
              }
            }
          }
          if (!$found) {
            $unknown[] = [
              $project_key,
              $version,
              $this->t('Unable to find release information'),
            ];
          }
        }
        else {
          $unknown[] = [
            $project_key,
            '',
            $this->t('No version information in module info'),
          ];
        }
      }
    }
    $header = [
      $this->t('Module name'),
      $this->t('Version'),
      $this->t('Status'),
    ];
    return [
      [
        '#markup' => '<h2>' . $this->t('Not opted-in') . '</h2>',
      ],
      [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $not_covered,
      ],
      [
        '#markup' => '<h2>' . $this->t('Opted-in') . '</h2>',
      ],
      [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $covered,
      ],
      [
        '#markup' => '<h2>' . $this->t('Unknown') . '</h2>',
      ],
      [
        '#type' => 'table',
        '#header' => [
          $this->t('Module name'),
          $this->t('Version'),
          $this->t('Error'),
        ],
        '#rows' => $unknown,
      ],
    ];

  }

}
